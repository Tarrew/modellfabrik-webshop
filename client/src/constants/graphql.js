import gql from "graphql-tag";

export const GET_SETTINGS = gql`
  query settings {
    settings {
      cylinders {
        height
        colors {
          r
          g
          b
          name
        }
      }
      maxHeight
      maxCylinders
    }
  }
`;

export const GET_ORDERS = gql`
  query orders($page: Int, $limit: Int) {
    orders(page: $page, limit: $limit) {
      _id
      leftStack {
        position
        height
        color {
          r
          g
          b
          name
        }
      }
      rightStack {
        position
        height
        color {
          r
          g
          b
          name
        }
      }
      orderName
      status
      createdAt
      updatedAt
    }
  }
`;

export const PRODUCE_ITEM = gql`
  mutation produceItem($order: ProductionOrder!) {
    produce(order: $order) {
      code
      msg
    }
  }
`;

export const ORDER_CREATED = gql`
  subscription orderCreated {
    orderCreated {
      _id
      leftStack {
        position
        height
        color {
          r
          g
          b
          name
        }
      }
      rightStack {
        position
        height
        color {
          r
          g
          b
          name
        }
      }
      orderName
      status
      createdAt
      updatedAt
    }
  }
`;
export const ORDER_CHANGED = gql`
  subscription orderChanged {
    orderChanged {
      _id
      leftStack {
        position
        height
        color {
          r
          g
          b
          name
        }
      }
      rightStack {
        position
        height
        color {
          r
          g
          b
          name
        }
      }
      orderName
      status
      createdAt
      updatedAt
    }
  }
`;

export const GET_STOCK = gql`
  query stock {
    stock {
      immediateStock {
        height
        color
        available
      }
      intermediateStock {
        height
        color
        available
      }
    }
  }
`;

export const STOCK_CHANGED = gql`
  subscription stockChanged {
    stockChanged {
      immediateStock {
        height
        color
        available
      }
      intermediateStock {
        height
        color
        available
      }
    }
  }
`;
