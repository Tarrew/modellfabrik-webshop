export const OrderStatus = Object.freeze({
  PENDING: "PENDING",
  IN_PRODUCTION: "IN_PRODUCTION",
  FINISHED: "FINISHED",
});
