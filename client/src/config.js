export const GRAPHQL_CONFIG = {
  httpLink: "https://modellfabrik-webshop-backend.herokuapp.com/graphql",
  wsLink: "wss://modellfabrik-webshop-backend.herokuapp.com/graphql",
  //httpLink: "http://localhost:3000/graphql",
  //wsLink: "ws://localhost:3000/graphql"
};
