import { Vector3, MeshBuilder, CSG, Mesh, Color3 } from "@babylonjs/core";
import { xRight, xLeft } from "../../shared/constants";
export function createCylinder(height, color, material, scene) {
  var cylinder = createCsgCylinder(height, material, scene);
  cylinder.height = height;
  cylinder.material.baseColor = new Color3(color.r, color.g, color.b);
  return cylinder;
}

function createCsgCylinder(height, material, scene) {
  //für sichtbare Abstände zwischen den gestapelten Zylindern
  height -= 0.75;

  var cylinderDiameter = 40;
  var tipHeight = 5;
  var tipDiameterBottom = 20;
  var tipDiameterTop = 14;
  var cylinderTesselation = 24;
  var coneTesselation = 24;

  var bottom = MeshBuilder.CreateCylinder(
    "cylinder",
    {
      height: height,
      diameterBottom: cylinderDiameter,
      diameterTop: cylinderDiameter,
      tessellation: cylinderTesselation,
    },
    scene
  );
  var top = MeshBuilder.CreateCylinder(
    "cone",
    {
      height: tipHeight,
      diameterBottom: tipDiameterBottom,
      diameterTop: tipDiameterTop,
      tessellation: coneTesselation,
    },
    scene
  );
  top.position.y = height / 2 + tipHeight / 2 - height;
  var bottomCsg = CSG.FromMesh(bottom);
  var topCsg = CSG.FromMesh(top);
  var sub = bottomCsg.subtract(topCsg);
  var bottomNew = sub.toMesh("csg", material, scene);
  top.position.y = height / 2 + tipHeight / 2;
  var csgCylinder = Mesh.MergeMeshes([bottomNew, top]);
  csgCylinder.material = material;
  scene.removeMesh(bottom);

  return csgCylinder;
}

function createLatheCylinder(height, material, scene) {
  var shape = [
    new Vector3(0, 5, 0),
    new Vector3(7, 5, 0),
    new Vector3(10, 0, 0),
    new Vector3(20, 0, 0),
    new Vector3(20, height, 0),
    new Vector3(10, height, 0),
    new Vector3(7, height + 5, 0),
    new Vector3(0, height + 5, 0),
    new Vector3(0, 5, 0),
  ];
  var latheCylinder = MeshBuilder.CreateLathe(
    "cylinder_" + height,
    { shape: shape },
    scene
  );
  latheCylinder.material = material;
  return latheCylinder;
}

export function loadPlateMesh(assetLoader, callback, plateMaterial) {
  var plate = assetLoader.addMeshTask("", "", "assets/", "plate.obj");
  plate.onSuccess = function() {
    var plateMesh = plate.loadedMeshes[0];
    plateMesh.scaling = new Vector3(0.7, 0.7, 0.7);
    plateMesh.setPositionWithLocalVector(new Vector3(-75, 0, 0));
    plateMesh.rotation.x = -Math.PI / 2;
    plateMesh.material = plateMaterial;

    callback(plateMesh);
  };
}

export function loadSymbolAMesh(assetLoader, callback) {
  var symbolA = assetLoader.addMeshTask("", "", "assets/", "A.stl");
  symbolA.onSuccess = function() {
    var symbolAMesh = symbolA.loadedMeshes[0];

    symbolAMesh.rotation.x = -Math.PI / 2;
    symbolAMesh.position = new Vector3(xLeft, 70, 4);
    symbolAMesh.scaling = new Vector3(2, 2, 2);
    callback(symbolAMesh);
  };
}

export function loadSymbolBMesh(assetLoader, callback) {
  var symbolB = assetLoader.addMeshTask("", "", "assets/", "B.stl");
  symbolB.onSuccess = function() {
    var symbolBMesh = symbolB.loadedMeshes[0];

    symbolBMesh.rotation.x = -Math.PI / 2;
    symbolBMesh.position = new Vector3(xRight, 70, 4);
    symbolBMesh.scaling = new Vector3(2, 2, 2);
    callback(symbolBMesh);
  };
}
 