import { Color3, CubeTexture } from "@babylonjs/core";
import { PBRMetallicRoughnessMaterial } from "@babylonjs/core/Materials/PBR/pbrMetallicRoughnessMaterial";

export function createPlateMaterial(scene) {
  var plateMaterial = new PBRMetallicRoughnessMaterial("plateMaterial", scene);
  plateMaterial.baseColor = new Color3(0.85, 0.85, 0.85);
  plateMaterial.metallic =0.8;
  plateMaterial.roughness = 0.15;
 // plateMaterial.emissiveColor = new Vector3(0.3, 0.3, 0.3);
  plateMaterial.environmentTexture = CubeTexture.CreateFromPrefilteredData("/assets/environment.dds", scene);
  return plateMaterial;
}

export function createCylinderMaterial(scene) {
  var cylinderMaterial = new PBRMetallicRoughnessMaterial(
    "cylinderMaterial",
    scene
  );

  cylinderMaterial.metallic = 0.8;
  cylinderMaterial.roughness = 0.15;
  cylinderMaterial.environmentTexture = CubeTexture.CreateFromPrefilteredData("/assets/environment.dds", scene);
  return cylinderMaterial;
}
