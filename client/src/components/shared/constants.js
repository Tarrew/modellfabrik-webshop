export const StatusEnum = Object.freeze({
  NONE: 1,
  PLACING_CYLINDER: 2,
  CYLINDER_SELECTED: 3,
});

export const xRight = 52.5;
export const xLeft = -52.5;