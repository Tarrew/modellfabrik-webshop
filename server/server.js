// Libs
const express = require("express");
const http = require("http");
const cors = require("cors");
const mongoose = require("mongoose");
const { ApolloServer } = require("apollo-server-express");

// Eigene Klassen
const { resolvers } = require("./graphql/resolvers");
const { OrderStatus } = require("./constants");
const { Order } = require("./data/schema");
const { schema } = require("./graphql/schema");
const {DB_URL} = require("./config");

const app = express();
app.use(cors());

const server = new ApolloServer({ typeDefs: schema, resolvers: resolvers });
server.applyMiddleware({
  app,
});

var port = process.env.PORT || 3000;
const httpServer = http.createServer(app);
server.installSubscriptionHandlers(httpServer);
httpServer.listen(port, () => {
  console.log(
    `🚀 Server ready at http://localhost:${port}${server.graphqlPath}`
  );
  console.log(
    `🚀 Subscriptions ready at ws://localhost:${port}${server.subscriptionsPath}`
  );
});

const uri = DB_URL;
mongoose
  .connect(uri, { useUnifiedTopology: true, useNewUrlParser: true })
  .catch((error) => console.log(error));
const connection = mongoose.connection;

mongoose.connection.on("error", (err) => {
  console.log(err);
});
connection.once("open", function () {
  console.log("MongoDB database connection established successfully");
});

pollOrders();

function pollOrders() {
  setTimeout(() => {
    Order.findOne({ status: OrderStatus.PENDING }, null, {
      sort: { createdAt: "asc" },
    }).then((currentOrder) => {
      if (currentOrder) {
        console.log("producing item");
        produceItem(currentOrder);
      } else {
        console.log("No pending order found");
        pollOrders();
      }
    });
  }, 5000);
}

async function produceItem(order) {
  order.status = OrderStatus.IN_PRODUCTION;
  await order.save();

  //Simulate the production process
  setTimeout(() => {
    order.status = OrderStatus.FINISHED;
    order.save().then((doc) => {
      console.log("finished production");
      pollOrders();
    });
  }, 30 * 1000);
}

const { pubsub, STOCK_CHANGED } = require("./graphql/pubsub");
const { getCurrentStock } = require("./factory/stock");
// Simulate new stock every 10 seconds
setInterval(function () {
  pubsub.publish(STOCK_CHANGED, { stockChanged: getCurrentStock() });
}, 10 * 1000);
