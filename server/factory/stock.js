function getCurrentStock() {
  return {
    immediateStock: [
      {
        height: 15,
        color: "silber",
        available: Math.floor(Math.random() * 5),
      },
      {
        height: 15,
        color: "blau",
        available: Math.floor(Math.random() * 5),
      },
      {
        height: 18,
        color: "silber",
        available: Math.floor(Math.random() * 5),
      },
      {
        height: 18,
        color: "blau",
        available: Math.floor(Math.random() * 5),
      },
      {
        height: 22,
        color: "silber",
        available: Math.floor(Math.random() * 5),
      },
      {
        height: 22,
        color: "blau",
        available: Math.floor(Math.random() * 5),
      },
    ],
    intermediateStock: [
      {
        height: 15,
        color: "silber",
        available: Math.floor(Math.random() * 5),
      },
      {
        height: 15,
        color: "blau",
        available: Math.floor(Math.random() * 5),
      },
      {
        height: 18,
        color: "silber",
        available: Math.floor(Math.random() * 5),
      },
      {
        height: 18,
        color: "blau",
        available: Math.floor(Math.random() * 5),
      },
      {
        height: 22,
        color: "silber",
        available: Math.floor(Math.random() * 5),
      },
      {
        height: 22,
        color: "blau",
        available: Math.floor(Math.random() * 5),
      },
    ],
  };
}

module.exports = {
  getCurrentStock,
};
