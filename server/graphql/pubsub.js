const { PubSub } = require("apollo-server-express");
const pubsub = new PubSub();
const ORDER_CREATED = "ORDER_CREATED";
const ORDER_CHANGED = "ORDER_CHANGED";
const STOCK_CHANGED = "STOCK_CHANGED";
module.exports = {
  pubsub,
  ORDER_CREATED,
  ORDER_CHANGED,
  STOCK_CHANGED
};
