var { Order } = require("../data/schema");
const { settings } = require("../config.js");
const { OrderStatus, Pagination } = require("../constants.js");
const { pubsub, ORDER_CREATED, ORDER_CHANGED, STOCK_CHANGED } = require("./pubsub");
const { getCurrentStock } = require("../factory/stock");

module.exports = {
  resolvers: {
    Query: {
      settings: () => {
        return settings;
      },
      async orders(parent, args, context, info) {
        var orderAggregate = Order.aggregate();
        const options = {
          page: args.page || Pagination.page,
          limit: args.limit || Pagination.limit,
          sort: { createdAt: "desc" },
        };
        return await Order.aggregatePaginate(orderAggregate, options)
          .then(function (result) {
            return result.docs;
          })
          .catch(function (err) {
            return null;
          });
      },
      stock: () => getCurrentStock(),
    },

    Mutation: {
      produce: async (_, { order }) => {
        //Hier muss der Auftrag in die SPS geschrieben werden

        order.status = OrderStatus.PENDING;
        return await new Order(order)
          .save()
          .then((order) => {
            pubsub.publish(ORDER_CREATED, { orderCreated: order });

            return { code: 0, msg: "Auftrag erfolgreich angelegt" };
          })
          .catch((error) => {
            return {
              code: 500,
              msg: "Auftrag konnte nicht angelegt werden: " + error,
            };
          });
      },
    },

    Subscription: {
      orderCreated: {
        subscribe: () => pubsub.asyncIterator([ORDER_CREATED]),
      },

      orderChanged: {
        subscribe: () => pubsub.asyncIterator([ORDER_CHANGED]),
      },

      stockChanged: {
        subscribe: () => pubsub.asyncIterator([STOCK_CHANGED]),
      },
    },
  },
};
