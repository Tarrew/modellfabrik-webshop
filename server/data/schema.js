var mongoose = require("mongoose");
var aggregatePaginate = require("mongoose-aggregate-paginate-v2");
const { pubsub, ORDER_CHANGED } = require("../graphql/pubsub");

var Schema = mongoose.Schema;

var orderSchema = new Schema(
  {
    leftStack: [
      {
        position: Number,
        height: Number,
        color: {
          r: Number,
          g: Number,
          b: Number,
          name: String,
        },
      },
    ],
    rightStack: [
      {
        position: Number,
        height: Number,
        color: {
          r: Number,
          g: Number,
          b: Number,
          name: String,
        },
      },
    ],
    status: String,
    orderName: String
  },
  { timestamps: true }
);
orderSchema.plugin(aggregatePaginate);
var Order = mongoose.model("Order", orderSchema, "orders");

Order.watch({ fullDocument: "updateLookup" }).on("change", (data) => {
  if (data.operationType == "update") {
    pubsub.publish(ORDER_CHANGED, { orderChanged: data.fullDocument });
  }
});

module.exports = {
  Order
}