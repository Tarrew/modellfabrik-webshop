module.exports = {
  DB_URL:"mongodb+srv://mfa:5ZLqo3tmxpc1togL@cluster0.iqtkb.mongodb.net/modellfabrik?retryWrites=true&w=majority",
  //DB_URL:"mongodb://localhost:27017/modellfabrik",
  settings: {
    cylinders: [
      {
        height: 15,
        colors: [
          {
            r: 0.85,
            g: 0.85,
            b: 0.85,
            name: "silber",
          },
          {
            r: 0.1,
            g: 0.1,
            b: 0.85,
            name: "blau",
          },
        ],
      },
      {
        height: 18,
        colors: [
          {
            r: 0.85,
            g: 0.85,
            b: 0.85,
            name: "silber",
          },
          {
            r: 0.1,
            g: 0.1,
            b: 0.85,
            name: "blau",
          },
        ],
      },
      {
        height: 22,
        colors: [
          {
            r: 0.85,
            g: 0.85,
            b: 0.85,
            name: "silber",
          },
          {
            r: 0.1,
            g: 0.1,
            b: 0.85,
            name: "blau",
          },
        ],
      },
    ],
    maxHeight: 100,
    maxCylinders: 5,
  },
};
