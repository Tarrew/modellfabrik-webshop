module.exports = {
  OrderStatus: Object.freeze({
    PENDING: "PENDING",
    IN_PRODUCTION: "IN_PRODUCTION",
    FINISHED: "FINISHED",
  }),

  Pagination: {
    page: 1,
    limit: 10,
  },
};
